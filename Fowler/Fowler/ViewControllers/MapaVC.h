//
//  MapaVC.h
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapaVC : UIViewController

@property (nonatomic, strong) NSArray *datos;


-(void)setTextoLblDatos:(NSString *)litros velocidad:(NSString *)velocidad fecha:(NSString *)fecha;
-(void) enfocarPOIEnMapa:(double)lat longitud:(double)lon;

@end
