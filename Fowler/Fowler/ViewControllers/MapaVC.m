//
//  MapaVC.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import "MapaVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Fowler-Swift.h"
#import "DatosApp.h"
#import "Color.h"

@interface MapaVC ()
@property (weak, nonatomic) IBOutlet UIView *vistaMapa;
@property (weak, nonatomic) IBOutlet UILabel *lblLitros;
@property (weak, nonatomic) IBOutlet UILabel *lblVelocidad;
@property (weak, nonatomic) IBOutlet UILabel *lblFecha;

@end

@implementation MapaVC{
    GMSMutablePath *ruta;
    GMSMapView *mapa;
    GMSCameraPosition *camara;
    GMSCoordinateBounds *bounds;
    GMSMarker *marcador;//Marcador unico que se muestra en en mapa al interactuar con la gráfica
}
@synthesize datos;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self inicializarValores];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void) dibujarPolilinea:(GMSPath *)rutaPoligono velocidad:(double)vel{
    if(rutaPoligono != nil){
        GMSPolyline *polilinea = [GMSPolyline polylineWithPath:rutaPoligono];
        //polilinea.strokeColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        polilinea.strokeColor = [Color getColorPolilinea:vel];
        polilinea.strokeWidth = 3.0f;
        polilinea.map = mapa;
    }
}

-(void) dibujarRutaTractor{
    if(datos != nil){
        for(NSDictionary *dic in datos){
            GMSMutablePath *rutaTemp = [GMSMutablePath path];
            double lat = [[dic objectForKey:@"latitud"] doubleValue];
            double lon = [[dic objectForKey:@"longitud"] doubleValue];
            double velocidad = [[dic objectForKey:@"velocidad"] doubleValue];
            double latAnterior = 0, lonAnterior = 0;
            
            if([datos indexOfObject:dic] > 0){
                NSInteger indice = [datos indexOfObject:dic] - 1;
                NSDictionary *datoAnterior = [datos objectAtIndex:indice];
                latAnterior = [[datoAnterior objectForKey:@"latitud"] doubleValue];
                lonAnterior = [[datoAnterior objectForKey:@"longitud"] doubleValue];
                [rutaTemp addCoordinate:CLLocationCoordinate2DMake(latAnterior, lonAnterior)];
            }
            
            [rutaTemp addCoordinate:CLLocationCoordinate2DMake(lat, lon)];
            
            //            NSLog(@"%lu",rutaTemp.count);
            if(rutaTemp.count > 1){
                //Solo dibuja la ruta en caso de que la latitud o logitud sea diferente a la anterior
                if(latAnterior != lat || lonAnterior != lon){
                    [self dibujarPolilinea:rutaTemp velocidad:velocidad];
                }
            }
        }
    }
}

//Enfoca la camara del mapa en la ruta que recibe como parametro
-(void) enfocarPoligono:(GMSPath *)rutaAEnfocar{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithPath:rutaAEnfocar];
    GMSCameraUpdate *camPoligono = [GMSCameraUpdate fitBounds:bounds withPadding:50.0f];
    [mapa animateWithCameraUpdate:camPoligono];
}

- (IBAction)Cerrar:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//Crea los marcadores basandose en los datos obtenidos del csv y los mete en "ruta"
-(void) crearMarcadores{
    
    for(NSDictionary *dic in datos){
        //        GMSMarker *marker = [[GMSMarker alloc] init];
        //        marker.position = CLLocationCoordinate2DMake([[dic objectForKey:@"latitud"] doubleValue], [[dic objectForKey:@"longitud"] doubleValue]);
        //        marker.title = [NSString stringWithFormat:@"%.02f km/h", [[dic objectForKey:@"velocidad"] doubleValue]];
        //        marker.snippet = [dic objectForKey:@"fecha"];;
        //        marker.map = mapView;
        double lat = [[dic objectForKey:@"latitud"] doubleValue];
        double lon = [[dic objectForKey:@"longitud"] doubleValue];
        [ruta addCoordinate:CLLocationCoordinate2DMake(lat, lon)];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"Segue"]){
        GraficaVC *g = segue.destinationViewController;
        g.datos = datos;
    }
}

// ---------- MÉTODOS DELEGATE ----------
-(void)setTextoLblDatos:(NSString *)litros velocidad:(NSString *)velocidad fecha:(NSString *)fecha{
    _lblLitros.text = litros;
    _lblVelocidad.text = velocidad;
    _lblFecha.text = fecha;
}



-(void) enfocarPOIEnMapa:(double)lat longitud:(double)lon{
    if(marcador.map != nil){
        marcador.map = nil;
    }
    marcador = [GMSMarker markerWithPosition:CLLocationCoordinate2DMake(lat, lon)];
    
    //    marcador.position = CLLocationCoordinate2DMake(lat, lon);
    //    marker.title = [NSString stringWithFormat:@"%.02f km/h", [[dic objectForKey:@"velocidad"] doubleValue]];
    //    marker.snippet = [dic objectForKey:@"fecha"];
    marcador.map = mapa;
    [mapa animateToLocation:CLLocationCoordinate2DMake(lat, lon)];
}

// ---------- INICIALIZACIÓN ----------
-(void) inicializarValores{
    DatosApp *da = [DatosApp sharedInstance];
    [da setDelegateMapaVC:self];
    
    ruta = [GMSMutablePath path];
    [self crearMarcadores];
    CGFloat ancho = self.view.bounds.size.width;
    CGFloat alto = self.vistaMapa.bounds.size.height;
    CGRect rect = CGRectMake(0, 0, ancho, alto);
    
    bounds = [[GMSCoordinateBounds alloc] includingPath:ruta];
    camara = [GMSCameraPosition cameraWithLatitude:0 longitude:0 zoom:0];
    mapa = [GMSMapView mapWithFrame:rect camera:camara];
    mapa.camera = camara;
    [mapa setMapType:kGMSTypeHybrid];
    
    //Renderizado del mapa
    //[self dibujarPolilinea:ruta];
    
    NSDate *inicioMetodo = [NSDate date];//PRUEBAS DE DESEMPEÑO
    [self dibujarRutaTractor];
    NSDate *finalMetodo = [NSDate date];//PRUEBAS DE DESEMPEÑO
    NSTimeInterval tiempoEjecusion = [finalMetodo timeIntervalSinceDate:inicioMetodo];//PRUEBAS DE DESEMPEÑO
    NSLog(@"Tiempo de renderizado de polylinea en el mapa: %fs", tiempoEjecusion);//PRUEBAS DE DESEMPEÑO
    
    [self enfocarPoligono:ruta];
    [self.vistaMapa addSubview:mapa];
}

@end
