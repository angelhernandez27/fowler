//
//  InicioVC.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import "InicioVC.h"
#import "SQL.h"
#import "Toast.h"
#import "MapaVC.h"
#import "Fowler-Swift.h"

@interface InicioVC (){
    NSMutableArray *dataArray;
}

@end

@implementation InicioVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [SQL sharedInstance];
    [SQL ejecutarQuery:@"delete from registro"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)importarCSV:(id)sender {
    /*
     UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.image"]
     inMode:UIDocumentPickerModeImport];
     documentPicker.delegate = self;
     documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
     [self presentViewController:documentPicker animated:YES completion:nil];
     */
    Enlace *solicitud = [[Enlace alloc] init];
    [solicitud llamarApiGoogleWithIdCarpeta:@"1xb6IGe_TLquSzfhMgF-dMOg7tYE58l-M"];
    NSString *resp = [solicitud getRespuesta];
    
    NSLog(@"Prueba dpm");
}

-(void)insertIntoDatabaseFromFile:(NSString*)path
{
    NSDate *inicioMetodo = [NSDate date];//PRUEBAS DE DESEMPEÑO
    
    NSString *fileDataString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSMutableArray *nombreColumnas = [[NSMutableArray alloc] init];
    NSMutableArray *datos = [[NSMutableArray alloc] init];
    
    //Gives u the array with all lines separated by new line.
    NSArray *linesArray=[fileDataString componentsSeparatedByString:@"\r\n"];
    int k = 0;
    
    double litros = 0;//TEMPORAL (En lo que se implementa litros en el csv)
    
    for (id string in linesArray){
        NSMutableDictionary *dicDatos = [[NSMutableDictionary alloc] init];
        //Si es la primera Fila no entra ya que son los nombres de las columnas
        if([linesArray indexOfObject:string] != 0){
            
            litros += [self valorRandom];
            
            //Gives u the number of items in one line separated by comma(,)
            NSString *lineString=[linesArray objectAtIndex:k];
            
            //Gives u the array of all components.
            NSArray *columnArray=[lineString componentsSeparatedByString:@","];
            
            //Get elements from array and create a query to add all into database.
            NSMutableString *writeString = [NSMutableString stringWithCapacity:0]; //la capacidad se extiende conforme se necesite
            NSString *strQuery = (k == 1)? @"insert into registro values(1," :
            @"insert into registro(latitud,longitud,velocidad,altitud,fecha,litros) values(";
            
            for (int i=0; i < [columnArray count]; i++) {
                NSString *valorColumna = [columnArray objectAtIndex:i];
                NSString *nomColumna = [nombreColumnas objectAtIndex:i];
                
                [dicDatos setObject:valorColumna forKey:nomColumna];
                //Si el dato insertado es una fecha le agrega las comillas sencillas
                if([valorColumna containsString:@"/"] || [valorColumna containsString:@":"]){
                    [writeString appendString:[NSString stringWithFormat:@"'%@',",[columnArray objectAtIndex:i]]];
                }else{
                    [writeString appendString:[NSString stringWithFormat:@"%@,",[columnArray objectAtIndex:i]]];
                }
                
                if(i == columnArray.count -1){
                    [writeString appendString:[NSString stringWithFormat:@"%.03f,",litros]];//TEMPORAL (para agregar litros)
                    NSNumber *numTemp = [[NSNumber alloc] initWithFloat:litros];
                    [dicDatos setObject:numTemp forKey:@"litros"];
                    
                    writeString = (NSMutableString *)[writeString substringToIndex:(writeString.length -1)];//Se le borra la ultima coma
                    strQuery = [NSString stringWithFormat:@"%@%@);",strQuery, writeString];//Se agregan los parametros y se cierra el parentesis del query
                    //[SQL ejecutarQuery:strQuery];
                    
                    [datos addObject:[dicDatos copy]];
                }
            }
            
        }else{
            NSString *lineString = [[linesArray objectAtIndex:k] lowercaseString];
            nombreColumnas = [[lineString componentsSeparatedByString:@","] mutableCopy];
        }
        
        k++;
    }
    
    //NSArray *respQuery = [SQL ejecutarSelect:@"registro"];
    NSDate *finalMetodo = [NSDate date];//PRUEBAS DE DESEMPEÑO
    NSTimeInterval tiempoEjecusion = [finalMetodo timeIntervalSinceDate:inicioMetodo];//PRUEBAS DE DESEMPEÑO
    NSLog(@"Tiempo de procesamiento de archivo: %fs", tiempoEjecusion);//PRUEBAS DE DESEMPEÑO
    
    MapaVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MapaVC"];
    //vc.datos = respQuery;
    vc.datos = datos;
    [self presentViewController:vc animated:YES completion:nil];
}

-(void) cargarDeBD{
    NSArray *respQuery = [SQL ejecutarSelect:@"registro"];
    MapaVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MapaVC"];
    vc.datos = respQuery;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)exportarCSV:(id)sender {
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"RDOS" withExtension:@"csv"]
                                                                                                  inMode:UIDocumentPickerModeExportToService];
    documentPicker.delegate = self;
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (IBAction)cargarCSV:(id)sender {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"RDOS" ofType:@"csv"];
    if(![SQL hayRegistros:@"select * from registro"]){
        [self insertIntoDatabaseFromFile:path];
    }else{
        [self cargarDeBD];
    }
}

/*
 *
 * Handle Incoming File
 *
 */
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        NSString *alertMessage = [NSString stringWithFormat:@"Importación exitosa %@", [url lastPathComponent]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Importar"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        });
    }
}

/*
 *
 * Cancelled
 *
 */
- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller {
    //[Toast mostrarAbajo:self mensaje:@"SE CANCELAAAAA"];
}

//Retorna un valor random entre .100 y .999
-(double) valorRandom{
    int rnd = 100 + arc4random() % (500 - 100);
    double valorRnd = (double) rnd / 1000;
    return valorRnd;
}

@end
