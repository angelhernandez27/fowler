//
//  GraficaVC.swift
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

import UIKit
import SwiftChart

protocol GraficaVCDelegate: class{
    func setTextoLblDatos(litros:String, velocidad:String, fecha:String)
    func enfocarPOIEnMapa(lat:Double, longitud:Double)
}

@objc class GraficaVC: UIViewController, ChartDelegate{
    
    var datos = [Dictionary<String, AnyObject>]()
    var valoresLitros: [Float] = []
    var valoresVelocidad: [Float] = []
    var fechasDatos = [String]()
    var valoresLatitud: [Float] = []
    var valoresLongitud: [Float] = []
    
    @IBOutlet weak var grafica: Chart!
    
    weak var delegate: MapaVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let methodStart = Date()//PRUEBAS DE DESEMPEÑO
        self.inicializarValores()
        let methodFinish = Date()//PRUEBAS DE DESEMPEÑO
        let executionTime = methodFinish.timeIntervalSince(methodStart)//PRUEBAS DE DESEMPEÑO
        print("Tiempo de carga de gráfica: \(executionTime)s")//PRUEBAS DE DESEMPEÑO
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let da = DatosApp.sharedInstance()
        delegate = da?.getDelegateMapaVC() as? MapaVC
    }
    
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Float, left: CGFloat) {
        for (seriesIndex, dataIndex) in indexes.enumerated() {
            if let value = chart.valueForSeries(seriesIndex, atIndex: dataIndex) {
                //print("Touched series: \(seriesIndex): data index: \(dataIndex!); series value: \(value); x-axis value: \(x) (from left: \(left))")
                let fecha:String = fechasDatos[dataIndex!]
                let velocidad:String = NSString(format: "%.3f", valoresVelocidad[dataIndex!]) as String
                let lat:Double = Double(valoresLatitud[dataIndex!])
                let lon:Double = Double(valoresLongitud[dataIndex!])
                delegate?.setTextoLblDatos("\(value) L", velocidad: "\(velocidad) km/h", fecha: "\(fecha)")
                delegate?.enfocarPOIEnMapa(Double(lat), longitud: Double(lon))
            }
        }
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        
    }
    
    func didEndTouchingChart(_ chart: Chart) {
        
    }
    
    func inicializarValores() -> Void{
        self.inicializarGrafica()
        //        delegateDispVC = da?.getDelegateDispositivosVC() as! DispositivoVC
        //        delegateContSensVC = da?.getDelegateContSensVC() as! ContenedorSensoresVC
    }
    
    func inicializarGrafica() -> Void{
        grafica.delegate = self
        var arrLitros = [Float]()
        
        for dic in datos{
            valoresLitros.append(dic["litros"] as! Float)
            valoresVelocidad.append((dic["velocidad"]?.floatValue)!)
            fechasDatos.append(dic["fecha"] as! String)
            valoresLatitud.append((dic["latitud"]?.floatValue)!)
            valoresLongitud.append((dic["longitud"]?.floatValue)!)
        }
        
        // ------------- SERIE NORMAL --------------
        //PREUBA 1
        //var valoresGrafica: [Float] = []
        
        let series = ChartSeries(valoresLitros)
        //let series = ChartSeries([29.2, 26.5, 28.5, 30.0, 30.0, 34.6, 34.6, 39.9, 43.5, 46.3, 50.3, 54.1, 54.5, 52.0, 52.2, 54.5, 56.7, 59.6, 59.4, 60.7, 61.6, 64.3, 65.9, 68.8, 69.0, 69.0, 59.5, 40.6, 35.5, 36.6, 35.6, 27.1, 25.5, 21.8, 18.9, 16.7, 15.3, 14.7, 18.2, 16.1, 16.6, 15.1, 15.5, 13.0, 13.0, 0, 0, 23.8, 20.1, 18.9, 18.5, 21.1, 22.6, 23.8, 30.6, 32.2, 37.5, 39.7, 42.5, 42.7, 44.4, 45.8, 46.9, 46.5, 48.9, 51.5, 53.8, 55.5, 56.5, 56.5, 58.5, 60.2, 61.8, 63.6, 64.9, 62.1, 62.0, 62.9, 62.9, 61.6, 62.7, 60.5, 50.8, 37.8, 31.3, 26.2, 25.0, 21.2, 17.9, 15.0, 13.8, 12.7, 11.1, 9.9, 9.3, 8.8, 8.4, 9.0, 9.3, 8.3, 7.4, 8.5, 8.0])
        
        series.area = true
        series.colorsAndArea = (
            above: ChartColors.verdeFuerteDany(),
            below: ChartColors.grisDany(),
            aboveAreaColor: ChartColors.verdeDebilDany(),
            belowColorArea: ChartColors.gris2Dany(),
            zeroLevel: 10
        )
        
        //PRUEBA 2
        //        let datos = [
        //            (x: 0.0, y: 0),
        //            (x: 1, y: 3.1),
        //            (x: 4, y: 2),
        //            (x: 5, y: 4.2),
        //            (x: 7, y: 5),
        //            (x: 9, y: 9),
        //            (x: 10, y: 8)
        //        ]
        //        let series = ChartSeries(data: datos)
        //        series.area = true
        
        // ------------- SERIE CON RELLENO --------------
        //PRUEBA 3
        //        let data = [
        //            (x: 0.0, y: 0),
        //            (x: 3, y: 2.5),
        //            (x: 4, y: 2),
        //            (x: 5, y: 2.3),
        //            (x: 7, y: 3),
        //            (x: 8, y: 2.2),
        //            (x: 9, y: 2.5)
        //        ]
        //        let series = ChartSeries(data: data)
        //        series.area = true
        //        series.color = ChartColors.grisFuerteDany()
        //series.color = UIColor.init(displayP3Red: 0.8666666667, green: 0.8666666667, blue: 0.8666666667, alpha: 1.0)
        
        
        // Use `xLabels` to add more labels, even if empty
        //grafica.xLabels = ["cola", "3", "6", "9", "12", "15", "18", "21", "24"]
        
        // Format the labels with a unit
        //grafica.xLabelsFormatter = { String(Int(round($1))) + "h" }
        
        // ------------- SERIE CON DOBLE COLOR --------------
        //        let datos: [Float] = [0, -2, -2, 3, -3, 4, 1, 0, -1]
        //        let series = ChartSeries(datos)
        //        series.area = true
        //
        //        //Setea los colores para el nivel arriba y abajo del limite asi como tambien el valor limite
        //        series.colors = (
        //            above: ChartColors.greenColor(),
        //            below: ChartColors.redColor(),
        //            zeroLevel: 1
        //        )
        //
        //        // Set minimum and maximum values for y-axis
        //        grafica.minY = -7
        //        grafica.maxY = 7
        //
        //        // Format y-axis, e.g. with units
        //        grafica.yLabelsFormatter = { String(Int($1)) +  "ºC" }
        
        // ------------- MULTIPLES SERIES --------------
        //        let series1 = ChartSeries([29.2, 26.5, 28.5, 30.0, 30.0, 34.6, 34.6, 39.9, 43.5, 46.3, 50.3, 54.1, 54.5, 52.0, 52.2, 54.5, 56.7, 59.6, 59.4, 60.7, 61.6, 64.3, 65.9, 68.8, 69.0, 69.0, 59.5, 40.6, 35.5, 36.6, 35.6, 27.1, 25.5, 21.8, 18.9, 16.7, 15.3, 14.7, 18.2, 16.1, 16.6, 15.1, 15.5, 13.0, 13.0])
        //        series1.color = ChartColors.blueColor()
        //        series1.area = true
        //
        //        let series2 = ChartSeries([23.8, 20.1, 18.9, 18.5, 21.1, 22.6, 23.8, 30.6, 32.2, 37.5, 39.7, 42.5, 42.7, 44.4, 45.8, 46.9, 46.5, 48.9, 51.5, 53.8, 55.5, 56.5, 56.5, 58.5, 60.2, 61.8, 63.6, 64.9, 62.1, 62.0, 62.9, 62.9, 61.6, 62.7, 60.5, 50.8, 37.8, 31.3, 26.2, 25.0, 21.2, 17.9, 15.0, 13.8, 12.7, 11.1, 9.9, 9.3, 8.8, 8.4, 9.0, 9.3, 8.3, 7.4, 8.5, 8.0])
        //        series2.color = ChartColors.greyColor()
        //        series2.area = true
        //
        //        // A partially filled series
        //        let series3 = ChartSeries([9, 8, 10, 8.5, 9.5, 10])
        //        series3.color = ChartColors.purpleColor()
        //        grafica.add([series1, series2, series3])
        
        grafica.showXLabelsAndGrid = false
        grafica.add(series)
    }
    
    func actualizarGrafica() -> Void{
        grafica.removeAllSeries()
        var valoresGrafica: [Float] = []
        
        //En caso de no tener valores le inserta uno para poder mostrar la grafica
        if(valoresGrafica.count == 0){
            valoresGrafica.append(0.0);
        }
        
        let series = ChartSeries(valoresGrafica)
        
        series.area = true
        series.colorsAndArea = (
            above: ChartColors.verdeFuerteDany(),
            below: ChartColors.grisDany(),
            aboveAreaColor: ChartColors.verdeDebilDany(),
            belowColorArea: ChartColors.gris2Dany(),
            zeroLevel: 10
        )
        
        grafica.showXLabelsAndGrid = false
        grafica.add(series)
    }
}
