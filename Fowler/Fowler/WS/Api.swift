//
//  Api.swift
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

import Foundation

public class Api{
    
    var ruta = "https://www.googleapis.com"//PRUEBAS
    var mensaje: String = ""
    var parametros: [String:String] = [:]//Guarda todos los valores a enviar junto con los nombres de sus respectivos parametros
    var cabeceras: [String:String] = [:]//Guarda los valores a enviar en la vabecera junto con los nombres de sus respectivos parametros
    
    public func opCiclosCultivo(carpeta : GetCarpeta, completionHandler: (GetRespuesta?, NSError?) -> Void) {
        //self.ruta += String(format: "/drive/v2/files/%@/children?key=%@", carpeta.ID!, carpeta.googleToken!)
        self.ruta += "/auth/drive/folders/1xb6IGe_TLquSzfhMgF-dMOg7tYE58l-M"
        cabeceras["Content-Type"] = "application/x-www-form-urlencoded"
        //cabeceras["Authorization"] = "AIzaSyB5x8UaP3z3lMuKS1HFVSXxPfn0y-RDa-A"
        
        self.llamarWS(tipoMetodo:"GET", completionHandler: { (syedabsarObj:ApiObjectBase?, error: NSError? )->Void in completionHandler(syedabsarObj  as? GetRespuesta,error) })
    }
    
    private func llamarWS(tipoMetodo:String, completionHandler: (ApiObjectBase?, NSError?) -> Void) {
        
        var strParametros : String = ""
        ruta = ruta.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed)!//Permite los espacios en los valores de los parametros de la url
        let request = NSMutableURLRequest(url: URL(string: ruta)!)
        var data = Data()
        
        if(!parametros.isEmpty){
            for param in parametros{
                if(!param.value.isEmpty){
                    strParametros += "\(param.key)=\(param.value)&"
                }
            }
            strParametros.remove(at: strParametros.index(before: strParametros.endIndex))
            data = strParametros.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        }
        
        request.httpMethod = tipoMetodo
        for (param, valor) in cabeceras{
            request.addValue(valor, forHTTPHeaderField: param)
        }
        
        request.httpBody = data
        // set up the session
        let config = URLSessionConfiguration.default
        let sesion = URLSession(configuration: config)
        
        let task = sesion.dataTask(with:request as URLRequest, completionHandler:{(data, response, error) in
            
            if error != nil {
                print("error=\(error)")//SOLO DEBUG
                return
            } else {
                let datastring : String = String(data: data!, encoding: String.Encoding.utf8)!//SOLO DEBUG
                print("RESPUESTA:\n\(datastring)")//SOLO DEBUG
                self.mensaje = datastring as String
            }
        })
        
        task.resume()
    }
    
}

/**
 Get Catalogo.
 */
@objc(GetCarpeta)
public class GetCarpeta : ApiObjectBase{
    /// idCarpeta
    var ID: String?
    
    /// token de google
    var googleToken:String?
}

/**
 Get Respuesta.
 */
@objc(GetRespuesta)
public class GetRespuesta : ApiObjectBase {
    /// Respuesta
    var Respuesta: String?
}

/**
 A generic base class for all Objects.
 */
public class ApiObjectBase : NSObject
{
    var xmlResponseString: String?
    class func cpKeys() -> Array <String>
    {
        return []
    }
    required override public init(){}
    class func newInstance() -> Self {
        return self.init()
    }
}
