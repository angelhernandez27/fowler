//
//  AppDelegate.h
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

