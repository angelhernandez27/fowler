//
//  SQL.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import "SQL.h"
#import <sqlite3.h>
#import "Tablas.h"

@implementation SQL

static NSArray *rutasArchivos;
static NSString *dirDocumento;
static NSString *ruta;

+(SQL *)sharedInstance{
    static dispatch_once_t onceToken;
    static SQL *instancia = nil;
    dispatch_once(&onceToken, ^{
        instancia = [[SQL alloc] init];
    });
    return instancia;
}

-(id) init{
    self = [super init];
    if(self){
        // Buscar el archivo de base de datos
        rutasArchivos = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        dirDocumento = [rutasArchivos objectAtIndex:0];
        ruta = [dirDocumento stringByAppendingPathComponent:@"BD.db"];
        //int valor = sqlite3_exec(bd, [@"PRAGMA journal_mode=WAL" UTF8String], NULL, NULL, NULL);
        //NSLog(@"---------- %d ------", valor);
        
        [self copiarDocumentoDBaDirectorio];
        [self crearCopiaBDEditable];
    }
    return self;
}

-(void)copiarDocumentoDBaDirectorio{
    // Check if the database file exists in the documents directory.
    NSString *destinationPath = [dirDocumento stringByAppendingPathComponent:@"BD.db"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:destinationPath]) {
        // The database file does not exist in the documents directory, so copy it from the main bundle now.
        NSString *sourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"BD.db"];
        NSError *error;
        [[NSFileManager defaultManager] copyItemAtPath:sourcePath toPath:destinationPath error:&error];
        
        // Check if any error occurred during copying and display it.
        if (error != nil) {
            NSLog(@"%@", [error localizedDescription]);
        }
    }
}

-(void)crearCopiaBDEditable{
    BOOL exito;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    exito = [fileManager fileExistsAtPath:ruta];
    
    // Si ya existe el archivo, no lo crea -_-
    if (exito){
        [self crearTablas];
        return;
    }
    
    // Crea el archivo en el dispositivo
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"fowler.db"];
    exito = [fileManager copyItemAtPath:defaultDBPath toPath:ruta error:&error];
    if (!exito){
        //NSAssert1(0, @"Error al crear archivo editable de la base de datos: %@", [error localizedDescription]);
        NSLog(@"Error al crear archivo editable de la base de datos: %@", [error localizedDescription]);
    }else{
        [self crearTablas];
    }
}

-(void) crearTablas{
    NSDictionary *tablas = [Tablas getDiccionarioTablas];
    static sqlite3 *bd = NULL;
    char* error;
    
    // Buscar el archivo de base de datos
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];//DISPOSITIVO
    //NSString *documentsDirectory = @"/Users/biobot.farm/Documents";//SOLO PRUEBAS
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"BD.db"];
    
    for(NSString *query in [tablas allValues]){
        
        // Abre el archivo de base de datos
        if (sqlite3_open_v2([path UTF8String], &bd, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK) {
            
            //Ejecuta el query
            if(sqlite3_exec(bd, [query UTF8String], NULL, NULL, &error) != SQLITE_OK){
                NSLog(@"Error al crear la tabla: %s", error);
            }
            
            sqlite3_close(bd);
        }else{
            NSLog(@"No se pudo acceder a la bd");
        }
    }
    
    sqlite3_close(bd);
}

//Valida mediante selects si alguna tabla contiene registros y retorna YES en dicho caso
+(BOOL) hayRegistros:(NSString *)query{
    static sqlite3 *bd = NULL;
    sqlite3_stmt *resultado;
    int count = (int)[query length];
    BOOL respuesta = NO;
    
    if (sqlite3_open_v2([ruta UTF8String], &bd, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK) {
        if(sqlite3_prepare(bd,[query UTF8String],count, &resultado,NULL) == SQLITE_OK ){
            if(sqlite3_step(resultado)==SQLITE_ROW){
                respuesta = YES;
            }
        }
    }
    
    sqlite3_close(bd);
    return respuesta;
}

+(NSArray *)ejecutarSelect:(NSString *)tabla{
    // Variables para realizar la consulta
    NSMutableArray *res = [[NSMutableArray alloc] init];
    static sqlite3 *bd;
    sqlite3_stmt *resultado;
    const char* siguiente;
    NSString *query = [NSString stringWithFormat:@"select * from %@", tabla];
    int count = (int)[query length];
    
    if (sqlite3_open([ruta UTF8String], &bd) == SQLITE_OK) {
        if ( sqlite3_prepare(bd,[query UTF8String],count,&resultado,&siguiente) == SQLITE_OK ){
            NSArray *nomColumnas = [self getNombresColumnas:tabla];
            
            // Recorre el resultado
            while (sqlite3_step(resultado)==SQLITE_ROW){
                NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
                
                for(int i = 0; i < nomColumnas.count; i++){
                    NSString *valor = [NSString stringWithUTF8String: (char *)sqlite3_column_text(resultado, i)];
                    //En caso de tener el valor "(null)" o "<null>" se setea a NSNull ya que el valor se insertara al diccionario
                    if([valor isEqualToString:@"(null)"] || [valor isEqualToString:@"<null>"]){
                        valor = [NSNull null];
                    }
                    [dic setObject:valor forKey:[nomColumnas objectAtIndex:i]];
                }
                [res addObject:dic];
            }
        }
    }
    
    sqlite3_close(bd);
    return [res copy];
}

+(NSArray *)getNombresColumnas:(NSString *)tabla{
    NSMutableArray *resp = [[NSMutableArray alloc] init];
    static sqlite3 *bd;
    sqlite3_stmt *resultado;
    const char* siguiente;
    NSString *query = @"";
    
    if([tabla containsString:@" "]){
        NSRange rango = [tabla rangeOfString:@" "];
        NSInteger index = rango.location;
        tabla = [tabla substringToIndex:index];
    }
    query = [NSString stringWithFormat:@"PRAGMA table_info(%@);", tabla];
    int count = (int)[query length];
    
    if (sqlite3_open([ruta UTF8String], &bd) == SQLITE_OK) {
        if ( sqlite3_prepare(bd,[query UTF8String],count,&resultado,&siguiente) == SQLITE_OK ){
            while (sqlite3_step(resultado)==SQLITE_ROW){
                NSString *nomColumna = [NSString stringWithFormat:@"%s", (char *)sqlite3_column_text(resultado, 1)];
                [resp addObject:nomColumna];
            }
        }
    }
    // Cierra el archivo de base de datos
    sqlite3_close(bd);
    
    return [resp copy];
}

//SOLO PARA INSERTS, DELETES Y UPDATES
+(void) ejecutarQuery:(NSString *)query{
    static sqlite3 *bd = NULL;
    char* error;
    
    // Abre el archivo de base de datos
    if (sqlite3_open_v2([ruta UTF8String], &bd, SQLITE_OPEN_READWRITE, NULL) == SQLITE_OK) {
        //Ejecuta el query
        if(sqlite3_exec(bd, [query UTF8String], NULL, NULL, &error) != SQLITE_OK){
            NSLog(@"Error al ejecutar el query: %s", error);
        }
    }else{
        NSLog(@"No se pudo acceder a la bd");
    }
    
    sqlite3_close(bd);
}

//PRUEBA
+(NSArray *) getDatosDesdeBD:(NSString *)nomClase param:(NSString *)param{
    if([nomClase isEqualToString:@"MenuVC"]){
        return [self ejecutarSelect:@"predio"];
    }else if([nomClase isEqualToString:@"PredioVC"]){
        NSString *consulta = [NSString stringWithFormat:@"cultivo where predio_id = %@ and fecha_final > (select date('now'))", param];
        return [self ejecutarSelect:consulta];
    }else if([nomClase isEqualToString:@"PredioVC/Inactivos"]){
        NSString *consulta = [NSString stringWithFormat:@"cultivo where predio_id = %@ and fecha_final < (select date('now'))", param];
        return [self ejecutarSelect:consulta];
    }else if([nomClase isEqualToString:@"BotlogsCultivosTBC"]){
        NSString *consulta = [NSString stringWithFormat:@"biodispositivos where id_cultivo = %@", param];
        return [self ejecutarSelect:consulta];
    }else if([nomClase isEqualToString:@"BotlogsCultivosTBC/bitacora"]){
        NSString *consulta = [NSString stringWithFormat:@"bitacora where id_cultivo = %@", param];
        return [self ejecutarSelect:consulta];
    }else if([nomClase isEqualToString:@"BiodispositivosVC"]){
        NSString *consulta = [NSString stringWithFormat:@"sensor where cultivos_id = %@", param];
        return [self ejecutarSelect:consulta];
    }
    
    return @[];
}

@end
