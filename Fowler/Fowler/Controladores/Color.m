//
//  Color.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Color.h"

@implementation Color

+(UIColor *) getColorPolilinea:(double)velocidad{
    UIColor *color = [UIColor clearColor];
    if(velocidad <= 5){//VERDE
        color = [UIColor colorWithRed:80.0f/255.0f green:169.0f/255.0f blue:1.0f/255.0f alpha:1.0f];
    }else if(velocidad > 5 && velocidad <= 10){//VERDE-AMARILLO
        color = [UIColor colorWithRed:163.0f/255.0f green:194.0f/255.0f blue:2.0f/255.0f alpha:1.0f];
    }else if(velocidad > 10 && velocidad <= 15){//AMARILLO
        color = [UIColor colorWithRed:254.0f/255.0f green:226.0f/255.0f blue:2.0f/255.0f alpha:1.0f];
    }else if(velocidad > 15 && velocidad <= 20){//AMARILLO-NARANJA
        color = [UIColor colorWithRed:251.0f/255.0f green:177.0f/255.0f blue:0.0f alpha:1.0f];
    }else if(velocidad > 20 && velocidad <= 25){//NARANJA
        color = [UIColor colorWithRed:245.0f/255.0f green:115.0f/255.0f blue:3.0f/255.0f alpha:1.0f];
    }else if(velocidad > 25 && velocidad <= 30){//ROJO-NARANJA
        color = [UIColor colorWithRed:234.0f/255.0f green:2.0f/255.0f blue:2.0f/255.0f alpha:1.0f];
    }else if(velocidad > 30){//ROJO
        color = [UIColor colorWithRed:214.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    }
    
    return color;
}

@end
