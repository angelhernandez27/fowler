//
//  Color.h
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Color : NSObject

+(UIColor *) getColorPolilinea:(double)velocidad;

@end
