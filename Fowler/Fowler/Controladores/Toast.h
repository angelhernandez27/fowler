//
//  Toast.h
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Toast : NSObject

+(void) mostrarAbajo:(UIViewController *)vc mensaje:(NSString *)msj;
+(void) mostrarCentro:(UIViewController *)vc mensaje:(NSString *)msj;
+(void) mostrarAbajo:(UIViewController *)vc mensaje:(NSString *)msj duracion:(int)dura;
+(void) mostrarCentro:(UIViewController *)vc mensaje:(NSString *)msj duracion:(int)dura;

@end
