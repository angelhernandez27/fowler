//
//  Tablas.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import "Tablas.h"

@implementation Tablas

+(NSDictionary *) getDiccionarioTablas{
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            @"CREATE TABLE IF NOT EXISTS `registro` ( `id` INTEGER PRIMARY KEY AUTOINCREMENT DEFAULT 1, `latitud` double DEFAULT 0, `longitud` double DEFAULT 0, `litros` double DEFAULT 0, `velocidad` double DEFAULT 0, `altitud` double DEFAULT 0, `fecha` date )"
            , @"datos", nil];
}

//+(NSDictionary *) getDiccionarioTablas{
//     return [[NSDictionary alloc] initWithObjectsAndKeys:
//     @"DROP TABLE `registro`", @"datos", nil];
//}

@end
