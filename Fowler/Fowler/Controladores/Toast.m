//
//  Toast.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import "Toast.h"
#import <UIKit/UIKit.h>

@implementation Toast

+(void) mostrarAbajo:(UIViewController *)vc mensaje:(NSString *)msj{
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:nil message:msj  preferredStyle:UIAlertControllerStyleActionSheet];
    [vc presentViewController:alerta animated:YES completion:nil];
    
    int duracion = 1; // duración en segundos
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duracion * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alerta dismissViewControllerAnimated:YES completion:nil];
        
    });
}

+(void) mostrarCentro:(UIViewController *)vc mensaje:(NSString *)msj{
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:nil message:msj  preferredStyle:UIAlertControllerStyleAlert];
    [vc presentViewController:alerta animated:YES completion:nil];
    
    int duracion = 1; // duración en segundos
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duracion * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        [alerta dismissViewControllerAnimated:YES completion:nil];
        
    });
}

+(void) mostrarAbajo:(UIViewController *)vc mensaje:(NSString *)msj duracion:(int)dura{
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:nil message:msj  preferredStyle:UIAlertControllerStyleActionSheet];
    [vc presentViewController:alerta animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, dura * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alerta dismissViewControllerAnimated:YES completion:nil];
    });
}

+(void) mostrarCentro:(UIViewController *)vc mensaje:(NSString *)msj duracion:(int)dura{
    UIAlertController *alerta = [UIAlertController alertControllerWithTitle:nil message:msj  preferredStyle:UIAlertControllerStyleAlert];
    [vc presentViewController:alerta animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, dura * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alerta dismissViewControllerAnimated:YES completion:nil];
    });
}

@end
