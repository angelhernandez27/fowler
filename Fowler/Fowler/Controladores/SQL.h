//
//  SQL.h
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SQL : NSObject

+(SQL *)sharedInstance;
+(NSArray *) getDatosDesdeBD:(NSString *)nomClase param:(NSString *)param;
+(void) ejecutarQuery:(NSString *)query;
+(BOOL) hayRegistros:(NSString *)query;
+(NSArray *)ejecutarSelect:(NSString *)tabla;

@end
