//
//  DatosApp.m
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import "DatosApp.h"

@implementation DatosApp{
    id delegateMapaVC;
}

+(DatosApp *) sharedInstance{
    static dispatch_once_t token;
    static DatosApp *instancia;
    dispatch_once(&token, ^{
        instancia = [[DatosApp alloc] init];
    });
    
    return instancia;
}

-(id) init{
    self = [super init];
    if(self){
        delegateMapaVC = nil;
    }
    
    return self;
}

-(void) setDelegateMapaVC:(id)del{
    delegateMapaVC = del;
}

-(id) getDelegateMapaVC{
    return delegateMapaVC;
}

@end
