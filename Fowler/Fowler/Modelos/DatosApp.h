//
//  DatosApp.h
//  Fowler
//
//  Created by biobot.farm on 11/09/18.
//  Copyright © 2018 biobot.farm. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatosApp : NSObject

+(DatosApp *) sharedInstance;
-(void) setDelegateMapaVC:(id)del;
-(id) getDelegateMapaVC;

@end
